## [1.0.0] – 2023-07-11

### Added
- Creation of an asset on default path, if no asset found in config
