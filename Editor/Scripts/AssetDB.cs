using System.IO;
using UnityEditor;
using UnityEngine;


namespace Valhalla.AssetManagement.Editor
{
	public static class AssetDB
	{
		public static TAsset GetAnyOrCreateAtPath<TAsset>(string defaultDirectoryPath, string defaultFileName)
			where TAsset : ScriptableObject
		{
			var typeName = typeof(TAsset).Name;
			
			if (!defaultFileName.EndsWith(".asset"))
				defaultFileName += ".asset";
			
			string defaultFilePath = Path.Combine(defaultDirectoryPath, defaultFileName);
			
			var assetIds = AssetDatabase.FindAssets($"t:{typeName}");

			if (assetIds.Length == 0)
			{
				Debug.LogWarning($"No modules config found. Creating new one at path <{defaultFilePath}>. You can safely move it wherever you want.");

				if (!Directory.Exists(defaultDirectoryPath))
					Directory.CreateDirectory(defaultDirectoryPath);
				
				Debug.Log($"Creating asset <{typeName}> at path <{defaultFilePath}>");
				
				var result = ScriptableObject.CreateInstance<TAsset>();
				AssetDatabase.CreateAsset(result, defaultFilePath);
				AssetDatabase.SaveAssets();

				return result;
			}
			else
			{
				var assetPath = AssetDatabase.GUIDToAssetPath(assetIds[0]);

				if (assetIds.Length > 1)
					Debug.LogWarning($"Found more than one {typeName} in project. Using first one at path <{assetPath}>.");

				return AssetDatabase.LoadAssetAtPath<TAsset>(assetPath);
			}
		}
	}
}
